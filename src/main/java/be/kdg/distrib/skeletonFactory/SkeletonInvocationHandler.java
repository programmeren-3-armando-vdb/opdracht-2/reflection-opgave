package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.domain.ReturnCalculator;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class SkeletonInvocationHandler implements InvocationHandler, Runnable {
    private final MessageManager messageManager;
    private final Object object;

    public SkeletonInvocationHandler(Object skelObj) {
        messageManager = new MessageManager();
        this.object = skelObj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        System.out.println("\tmethodName = " + methodName);
        if ("getAddress".equals(methodName)) {
            return messageManager.getMyAddress();
        } else if ("handleRequest".equals(methodName)) {
            MethodCallMessage methodCallMessage = (MethodCallMessage) args[0];
            handleRequest(methodCallMessage);
        } else if ("run".equals(methodName)) {
            Thread skelThread = new Thread(this);
            skelThread.start();
        }
        return null;
    }

    private void handleRequest(MethodCallMessage methodCallMessage) throws RuntimeException {
        String methodName = methodCallMessage.getMethodName();
        Class objClass = object.getClass();
        Method method = null;
        try {
            for (Method decMethod : objClass.getDeclaredMethods()) {
                if (decMethod.getName().equals(methodName)) {
                    method = decMethod;
                }
            }

            if (method == null) {
                throw new RuntimeException("Method unknown.");
            }

            Set<String> distinctArgs = new HashSet<>();
            for (String key : methodCallMessage.getParameters().keySet()) {
                distinctArgs.add(key.split("[.]")[0]);
            }
            if (distinctArgs.size() != method.getParameters().length) {
                throw new RuntimeException("Amount of parameters isn't matching");
            }
            Object[] parameters = new Object[method.getParameters().length];
            for (int i = 0; i < method.getParameters().length; i++) {
                parameters[i] = ReturnCalculator.getReturn(method.getParameters()[i].getType().getName(), methodCallMessage.getParameter("arg" + i));
                if (parameters[i] == null) {
                    parameters[i] = method.getParameters()[i].getType().getDeclaredConstructor().newInstance();
                    for (Field field : method.getParameters()[i].getType().getDeclaredFields()) {
                        field.setAccessible(true);
                        String argNumber = "arg" + i + ".";
                        field.set(parameters[i], ReturnCalculator.getReturn(field.getType().getName(), methodCallMessage.getParameter(argNumber + field.getName())));
                    }
                }
                if (parameters[i] == null) {
                    throw new RuntimeException("Parameter cannot be null");
                }
            }
            Object returnValue = method.invoke(object, parameters);
            if (returnValue != null) {
                MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
                if (returnValue.getClass().getName().startsWith("java.lang")) {
                    response.setParameter("result", returnValue.toString());
                } else {
                    Class returnClass = returnValue.getClass();
                    for (Field field :
                            returnClass.getDeclaredFields()) {
                        field.setAccessible(true);
                        response.setParameter("result." + field.getName(), field.get(returnValue).toString());
                    }
                }
                messageManager.send(response, methodCallMessage.getOriginator());
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        sendEmptyReply(methodCallMessage);
    }

    private void sendEmptyReply(MethodCallMessage methodCallMessage) {
        MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
        response.setParameter("result", "Ok");
        messageManager.send(response, methodCallMessage.getOriginator());
    }

    @Override
    public void run() throws RuntimeException {
        while (true) {
            MethodCallMessage response = messageManager.wReceive();
            handleRequest(response);
        }
    }
}

package be.kdg.distrib.skeletonFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class SkeletonFactory {
    public static Object createSkeleton(Object skelObj) {
        InvocationHandler invocationHandler = new SkeletonInvocationHandler(skelObj);
        Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),new Class[]{Skeleton.class},invocationHandler);
        return result;
    }
}

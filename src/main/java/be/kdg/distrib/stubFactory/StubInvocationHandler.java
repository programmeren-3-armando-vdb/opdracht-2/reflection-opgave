package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.domain.ReturnCalculator;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class StubInvocationHandler implements InvocationHandler {
    private final NetworkAddress networkAddress;

    public StubInvocationHandler(NetworkAddress networkAddress) {
        this.networkAddress = networkAddress;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        System.out.println("\tmethodName = " + methodName);

        MessageManager messageManager = new MessageManager();
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), methodName);

        if (args != null) {
            int i = 0;
            for (Object arg : args) {
                System.out.println("\targ" + i + ": " + arg.toString());
                if (arg.getClass().getName().startsWith("java.lang")) {
                    methodCallMessage.setParameter("arg" + i, String.valueOf(arg));
                } else {
                    for (Field field : arg.getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        methodCallMessage.setParameter("arg" + i + "." + field.getName(), String.valueOf(field.get(arg)));
                    }
                }
                i++;
            }
        }

        messageManager.send(methodCallMessage, networkAddress);
        MethodCallMessage response = messageManager.wReceive();

        Class returnType = method.getReturnType();
        if (returnType.getName().equals("void")) {
            return null;
        } else {
            Object temp = ReturnCalculator.getReturn(returnType.getName(), response.getParameter("result"));
            if (temp != null) {
                return temp;
            } else {
                Object object = method.getReturnType().getDeclaredConstructor().newInstance();
                for (Field field : returnType.getDeclaredFields()) {
                    field.setAccessible(true);
                    field.set(object, ReturnCalculator.getReturn(field.getType().getName(), response.getParameter("result." + field.getName())));
                }
                return object;
            }
        }
    }
}

package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.*;

public class StubFactory {
    public static Object createStub(Class stubClass, String address, int port) {
        NetworkAddress networkAddress = new NetworkAddress(address,port);
        InvocationHandler handler = new StubInvocationHandler(networkAddress);
        if (stubClass.isInterface()) {
            return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{stubClass}, handler);
        } else {
            throw new IllegalArgumentException("Can't create stub from Object.");
        }
    }
}

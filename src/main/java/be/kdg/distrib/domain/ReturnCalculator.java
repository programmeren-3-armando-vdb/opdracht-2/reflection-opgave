package be.kdg.distrib.domain;

public class ReturnCalculator {
    public static Object getReturn(String type, String messageValue) {
        switch (type) {
            case "int":
            case "java.lang.Integer":
                return Integer.parseInt(messageValue);
            case "double":
            case "java.lang.Double":
                return Double.parseDouble(messageValue);
            case "java.lang.String":
                return messageValue;
            case "char":
            case "java.lang.Character":
                return messageValue.charAt(0);
            case "boolean":
            case "java.lang.Boolean":
                return Boolean.parseBoolean(messageValue);
            case "byte":
            case "java.lang.Byte":
                return Byte.parseByte(messageValue);
            case "short":
            case "java.lang.Short":
                return Short.parseShort(messageValue);
            default:
                return null;
        }
    }
}
